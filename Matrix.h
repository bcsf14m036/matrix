#include<iostream>
#include"ConsoleFunctions.h"

#ifndef MATRIX_H
#define MATRIX_H


class Node
{
	int row;
	int col;
	int val;
	Node * nextRow;
	Node * nextCol;
	friend class Matrix;
public:
	Node();
	Node(int r,int c,int v,Node* ro,Node* co);
	Node(int r,int c,int v);

};

class Matrix
{
	Node* head;
	int maxRows;
	int maxCols;
	ConsoleFunctions consoleHandle;
public:
	Matrix();
	void InsertCell(int r, int c, int val);
	Node* isColoumnExist(int c);
	Node* isRowExist(int r);
	bool isExisting(int r, int c);
	void RemoveCell(int r, int c, int val);
	Node* getPointertoCell(int r, int c);
	void display();

};

#endif