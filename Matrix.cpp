#include<iostream>
#include"ConsoleFunctions.h"
#include"Matrix.h"
using namespace std;

Node::Node()
{
	row=-1;
	col=-1;
	val=-1;
	nextCol=NULL;
	nextRow=NULL;
}
Node::Node(int r,int c,int v,Node* ro,Node* co)
{
	row=r;
	col=c;
	val=v;
	nextCol=ro;
	nextRow=co;
}
Node::Node(int r,int c,int v)
{
	row=r;
	col=c;
	val=v;
	nextCol=NULL;
	nextRow=NULL;
}
Matrix::Matrix()
{
	head=new Node();
	maxRows=-1;
	maxCols=-1;
}

Node* Matrix:: isColoumnExist(int c)
{
	Node* ptr=head->nextCol;
	while(ptr!=NULL)
	{
		if(ptr->col==c)
		{
			return ptr;
		}
		ptr=ptr->nextCol;
	}
	return NULL;
}

Node* Matrix:: isRowExist(int r)
{
	Node* ptr=head->nextRow;
	while(ptr!=NULL)
	{
		if(ptr->row==r)
		{
			return ptr;
		}
		ptr=ptr->nextRow;
	}
	return NULL;
}

void Matrix::InsertCell(int r, int c, int val)
{
	Node * temp;
	Node* ansR=isRowExist(r);
	Node* ansC=isColoumnExist(c);
	Node * t= new Node(r,c,val);
	if(head->nextRow==NULL&&head->nextCol==NULL)
	{
		head->nextCol=new Node(-1,c,-1);
		head->nextRow=new Node(r,-1,-1);
		head->nextCol->nextRow=t;
		head->nextRow->nextCol=t;
		maxRows=r;
		maxCols=c;
	}
	else if(ansR==NULL && ansC!=NULL)
	{
		Node * ptr=head->nextRow;
		if(ptr->row>r)
		{
			head->nextRow=new Node(r,-1,-1);
			head->nextRow->nextRow=ptr;
			head->nextRow->nextCol=t;
		}
		else
		{
			while(ptr->nextRow->row<r && ptr->nextRow!=NULL)
			{
				ptr=ptr->nextRow;
			}
			temp=ptr->nextRow;
			ptr->nextRow=new Node(r,-1,-1);
			ptr->nextRow->nextRow=temp;
			ptr->nextRow->nextCol=t;
		}

			while(ansC->nextRow->row<r && ansC->nextRow!=NULL)
			{
				ansC=ansC->nextRow;
			}
			temp=ansC->nextRow;
			ansC->nextRow=t;
			ansC->nextRow->nextRow=temp;
	}
	else if(ansR!=NULL && ansC==NULL)
	{
		Node * ptr=head->nextCol;
		if(ptr->col>c)
		{
			head->nextCol=new Node(-1,-c,-1);
			head->nextCol->nextCol=ptr;
			head->nextCol->nextRow=t;
		}
		else
		{
			while(ptr->nextCol->col<c && ptr->nextCol!=NULL)
			{
				ptr=ptr->nextCol;
			}
			temp=ptr->nextCol;
			ptr->nextCol=new Node(-1,c,-1);
			ptr->nextCol->nextCol=temp;
			ptr->nextCol->nextRow=t;
		}
			while(ansR->nextCol->col<c && ansR->nextCol!=NULL)
			{
				ansR=ansR->nextCol;
			}
			temp=ansR->nextCol;
			ansR->nextCol=t;
			ansR->nextCol->nextCol=temp;
	}
	else if(ansC!=NULL && ansR!=NULL)
	{
			while(ansC->nextRow->row<r && ansC->nextRow!=NULL)
			{
				ansC=ansC->nextRow;
			}
			temp=ansC->nextRow;
			ansC->nextRow=t;
			ansC->nextRow->nextRow=temp;
			while(ansR->nextCol->col<c && ansR->nextCol!=NULL)
			{
				ansR=ansR->nextCol;
			}
			temp=ansR->nextCol;
			ansR->nextCol=t;
			ansR->nextCol->nextCol=temp;
	}
	else if(ansC==NULL,ansR==NULL)
	{
		Node * ptr=head->nextRow;
		if(ptr->row>r)
		{
			head->nextRow=new Node(r,-1,-1);
			head->nextRow->nextRow=ptr;
			head->nextRow->nextCol=t;
		}
		else
		{
			while(ptr->nextRow->row<r && ptr->nextRow!=NULL)
			{
				ptr=ptr->nextRow;
			}
			temp=ptr->nextRow;
			ptr->nextRow=new Node(r,-1,-1);
			ptr->nextRow->nextRow=temp;
			ptr->nextRow->nextCol=t;
		}
		ptr=head->nextCol;
		if(ptr->col>c)
		{
			head->nextCol=new Node(-1,-c,-1);
			head->nextCol->nextCol=ptr;
			head->nextCol->nextRow=t;
		}
		else
		{
			while(ptr->nextCol->col<c && ptr->nextCol!=NULL)
			{
				ptr=ptr->nextCol;
			}
			temp=ptr->nextCol;
			ptr->nextCol=new Node(-1,c,-1);
			ptr->nextCol->nextCol=temp;
			ptr->nextCol->nextRow=t;
		}
	}
}
bool Matrix :: isExisting(int r, int c)
{
	Node * check=NULL;
	Node* ansR=isRowExist(r);
	Node* ansC=isColoumnExist(c);
	if(ansR!=NULL && ansC!=NULL)
	{
		while(ansC->row<=r)
		{
			if(ansC->row==r)
			{
				if(ansC->col==c)
					return true;
			}
			ansC=ansC->nextRow;
		}
		return false;
	}
	else
		return false;
}
Node* Matrix::getPointertoCell(int r, int c)
{
	if(isExisting(r,c))
	{
		Node* ansC=isColoumnExist(c);
		while(ansC->row<=r)
		{
			if(ansC->row==r)
			{
				if(ansC->col==c)
					return ansC;
			}
			ansC=ansC->nextRow;
		}
	}
	return NULL;
}
void Matrix::RemoveCell(int r, int c, int val)
{
	if(isExisting(r, c))
	{
		Node* ansR=isRowExist(r);
		Node* ansC=isColoumnExist(c);
		while(ansC->nextRow->row<=r)
		{
			if(ansC->nextRow->row==r)
			{
				if(ansC->nextRow->col==c)
				{
						Node * tempR=ansC->nextRow;
						Node * tempC=ansC->nextCol;
						while(ansR->nextCol->col<=c)
						{
							if(ansR->nextCol->col==c)
							{
								if(ansR->nextCol->row==r)
								{
									delete ansC;
									ansC->nextRow=tempR;
									ansR->nextCol=tempC;
								}
							}
							ansR=ansR->nextCol;
						}
				}
			}
			ansC=ansC->nextRow;
		}
		cout<<"The cell has been deleted.	"<<endl;
	}
	else
		cout<<"Error; the cell does not exist."<<endl;
}
void Matrix::display()
{
	





}